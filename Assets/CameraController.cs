﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    private float rotX; // Yaw
    private float rotY; // Pitch
    private float rotZ; // Roll

    private const float speed = 1.0f;
    private const int col_dist = 12;

	void Start () {
        rotX = 0;
        rotY = 0;
        rotZ = 0;
	}
	
	void FixedUpdate () {

        rotX += Input.GetAxis("Mouse X") * speed;
        rotY += Input.GetAxis("Mouse Y") * speed;
        if (Input.GetKey(KeyCode.Q))
            rotZ += speed;
        if (Input.GetKey(KeyCode.E))
            rotZ -= speed;

        rotY = Mathf.Clamp(rotY, -90, 90);
        //rotZ = Mathf.Clamp(rotZ, -90, 90);


        transform.localRotation = Quaternion.AngleAxis(rotX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotY, Vector3.left);
        transform.localRotation *= Quaternion.AngleAxis(rotZ, Vector3.forward);

        float vertInput = Input.GetAxis("Vertical");
        float horInput = Input.GetAxis("Horizontal");
        bool rayCollide = false;
        rayCollide = Physics.Raycast(transform.position, transform.forward * vertInput, col_dist);
        if (!rayCollide)
        {
            transform.position += transform.forward * speed * vertInput;
        }

        rayCollide = Physics.Raycast(transform.position, transform.right * horInput, col_dist);
        if (!rayCollide)
        {
            transform.position += transform.right * speed * horInput;
        }

    }

    void OnTriggerEnter(Collider terrain)
    {
        Debug.Log("Collision with terrain.");
    }

    void OnTriggerExit(Collider terrain)
    {
        Debug.Log("Collision exit triggered.");
    }
}
