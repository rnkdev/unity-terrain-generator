﻿using UnityEngine;
using Assets;
using System.Collections;

public class HeightmapGenerator : MonoBehaviour {

    public Terrain mainTerrain;
    private TerrainData terrData;
    private float[,] heightMap;

	// Use this for initialization
	void Start () {

        terrData = mainTerrain.terrainData;
        DiamondAlgorithmGenMod dag = new DiamondAlgorithmGenMod(1.0f);
        heightMap = dag.generateMap(terrData.heightmapWidth, terrData.heightmapHeight);
        terrData.SetHeights(0, 0, heightMap);
    }

    // Update is called once per frame
	void Update () {
	
	}
}
