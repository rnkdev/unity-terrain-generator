﻿// Original Cg/HLSL code stub copyright (c) 2010-2012 SharpDX - Alexandre Mutel
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// Adapted for COMP30019 by Jeremy Nicholson, 10 Sep 2012
// Adapted further by Chris Ewin, 23 Sep 2013
// Adapted further (again) by Alex Zable (port to Unity), 19 Aug 2016

// Modified so that the shader works with proper Unity lightning and terrain object
// by Rizki Kadir (656588) with the help of Cg Programming/Unity/Smooth Specular Highlights wiki

// Modified to add texture
// Modified to do simple control based on heightmap

Shader "Custom/ModPhong-Tex" 
{
	Properties
	{
		_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
		
			#pragma vertex vert  
			#pragma fragment frag 

			#include "UnityCG.cginc"
			uniform float4 _LightColor0;
			uniform float4 _Color;

			struct vertIn 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct vertOut 
			{
				float4 vertex : SV_POSITION;
				float4 worldVertex : TEXCOORD3;
				float3 worldNormal : TEXCOORD1;
				float2 uv : TEXCOORD0;
				float hControl : TEXCOORD4;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			// Implementation of the vertex shader
			vertOut vert(vertIn v)
			{
				vertOut o;

				// Pass out the world vertex position and world normal to be interpolated
				// in the fragment shader -- Streamlined the passing

				o.worldVertex = mul(_Object2World, v.vertex);
				o.worldNormal = normalize(mul(transpose((float3x3)_World2Object), v.normal.xyz));

				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);

				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				// Height map control
				o.hControl = lerp(0.5, 1.0, step(v.vertex.y, 25));

				return o;
			}

			// Implementation of the fragment shader
			fixed4 frag(vertOut v) : SV_TARGET
			{
				float3 interpNormal = normalize(v.worldNormal);

				float3 viewDirection = normalize(
					_WorldSpaceCameraPos - v.worldVertex.xyz);
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float attenuation = 1.0; // no attenuation

				// Ambient Calculation
				float3 ambientLighting =  UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;

				// Diffuse Calculation
				float LdotN = dot(interpNormal, lightDirection);
				float3 diffuseReflection =	attenuation * _LightColor0.rgb * _Color.rgb
					* saturate(LdotN);

				float3 specularReflection;
				// Check that the light is on top of the target surface
				if (LdotN < 0.0)
				{
					specularReflection = float3(0.0, 0.0, 0.0);
				}
				else 
				{
					float specN = 2;
					float3 HVector = normalize(viewDirection + lightDirection);
					specularReflection = attenuation * _LightColor0.rgb
						* _Color.rgb * pow(saturate(dot(interpNormal, HVector)), specN);
				}

				float4 heightCtrl = float4(0.5, 0.7, 1.0, 1.0)*v.hControl;
				// Applying heightmap control
				fixed4 texCol = tex2D(_MainTex, v.uv)*v.hControl;

				return float4(ambientLighting + diffuseReflection
					+ specularReflection, 1.0)*texCol;
			}

		ENDCG
		}

	}
		Fallback "Specular"
}
