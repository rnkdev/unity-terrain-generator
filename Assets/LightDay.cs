﻿using UnityEngine;
using System.Collections;

public class LightDay : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        System.TimeSpan timespan = System.DateTime.Now.TimeOfDay;
        float rot = (float)timespan.TotalSeconds * (360f / 60f);
        transform.localRotation = Quaternion.Euler(rot, 0f, 0f);

    }
}
