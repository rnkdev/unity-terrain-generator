﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    class DiamondAlgorithmGenMod
    {
        private Random rand;

        private float maxHeight;

        private int maxY;
        private int maxX;

        /* TERR_ROUGHNESS sets the roughness of the terrain details, e.g fine desert has low value 
         * vs rough planetary surface has high value 
         */
        private const float TERR_ROUGHNESS = 1.86f;
        /* TERR_GRADIENT sets the steepness of major terrain features vs the rest of the terrain
         * e.g grassland will have mostly flat plain, mountain range will have 
         * mountain with oscillating heights
         */
        private const float TERR_GRADIENT = 0.89f;

        public DiamondAlgorithmGenMod()
        {
            rand = new Random();
            maxHeight = 0;
            maxY = 0;
            maxX = 0;
        }

        public DiamondAlgorithmGenMod(float mHeight) : this()
        {
            rand = new Random();
            maxHeight = mHeight;

        }

        /* We can supply the RNG with constant seed if we like to reproduce the same result */
        public DiamondAlgorithmGenMod(int seed, float mHeight)
        {
            rand = new Random(seed);
            maxHeight = mHeight;
        }

        public float[,] generateMap(int width, int height)
        {
            float[,] heightMap = new float[width, height];

            maxX = maxY = width;

            seedMap(heightMap);

            DiamondSquareRec(heightMap, width);

            normalizeMap(heightMap);
            cutOffEdges(heightMap);

            return heightMap;
        }

        private void DiamondSquareRec(float[,] heightMap, int size)
        {
            if (size / 2 < 1)
                return;
            // For debug
            // Console.WriteLine(size);

            // To handle non-square heightmap if necessary later, now it's a square only
            int ybound = size;
            int xbound = size;

            // Step through the available squares to do diamond averaging
            for (int y = ybound / 2; y < maxY; y += size)
            {
                for (int x = xbound / 2; x < maxX; x += size)
                {
                    diamondStep(heightMap, x, y, ybound / 2, getRandomHeight(size));
                }
            }

            // Step through the square pixel to do the squaring 
            for (int y = 0; y < maxY; y += (size / 2))
            {
                int lastindex = (size / 2) * 2; // Round off to even value if odd
                int xint = 0; // We need to shift the point to be squared 

                if (y % lastindex == 0)
                {
                    xint = size / 2;
                }
                else
                {
                    xint = 0;
                }

                for (int x = xint; x < maxX; x += lastindex)
                {
                    squareStep(heightMap, x, y, xbound / 2, getRandomHeight(size));
                }
            }

            DiamondSquareRec(heightMap, size / 2);

        }

        /* Perform diamond operation, where (x,y) is the center point,
         * depth is the distance between the centre and the corners 
         * avval is the value needed on top of the average 
         */
        private void diamondStep(float[,] heightMap, int x, int y, int depth, float avval)
        {

            float topl = heightMap[x - depth, y - depth];
            float topr = heightMap[x + depth, y - depth];

            float btml = heightMap[x - depth, y + depth];
            float btmr = heightMap[x + depth, y + depth];

            heightMap[x, y] = (((topl + topr + btml + btmr) * 0.25f) + avval);
        }

        /* Perform square operation, where (x,y) is the center point,
         * depth is the distance between the centre and the square corners 
         * avval is the value needed on top of the average 
         */
        private void squareStep(float[,] heightMap, int x, int y, int depth, float avval)
        {

            /* If the target vertex is in the corner, we will wrap-around the value from other corner */

            float topl = heightMap[wrapMod(x), wrapMod(y - depth)];
            float topr = heightMap[wrapMod(x + depth), wrapMod(y)];

            float btml = heightMap[wrapMod(x), wrapMod(y + depth)];
            float btmr = heightMap[wrapMod(x - depth), wrapMod(y)];

            heightMap[x, y] = (((topl + topr + btml + btmr) * 0.25f) + avval);
        }

        /* Perform square operation, where (x,y) is the center point,
         * depth is the distance between the centre and the square corners 
         * avval is the value needed on top of the average 
         * Non-Wrapping
         */
        private void squareStep2(float[,] heightMap, int x, int y, int depth, float avval)
        {

            /* If the target vertex is in the corner, we will wrap-around the value from other corner */
            float points = 0.0f;
            float sum = 0.0f;

            if (y - depth > 0)
                sum += heightMap[x, y - depth]; points++;
            if (x + depth < maxX - 1)
                sum += heightMap[x + depth, y]; points++;
            if (y + depth < maxX - 1)
                sum += heightMap[x, y + depth]; points++;
            if (x - depth > 0)
                sum += heightMap[x - depth, y]; points++;

            heightMap[x, y] = (sum / points) + avval;
        }

        private void seedMap(float[,] heightMap)
        {
            /* For simplifaction, we'll consider the heightMap to be square */
            int maxlength = heightMap.GetLength(0) - 1;

            float rval = getRandomHeight() * maxX * 0.25f;

            heightMap[0, 0] = rval;
            heightMap[0, maxlength] = rval;
            heightMap[maxlength, 0] = rval;
            heightMap[maxlength, maxlength] = rval;

        }

        /* We need to cutOffEdges in order to remove wrapping artefact at the edge
         * of the map */
        private void cutOffEdges(float[,] heightMap)
        {
            for (int i = 0; i < 1; i++)
            {
                // Top
                for (int x = 0; x < maxX; x++)
                {
                    heightMap[x, i] = heightMap[x, i+1];
                }

                // Left 
                for (int y = 0; y < maxY; y++)
                {
                    heightMap[i, y] = heightMap[1+i, y];
                }

                // Right
                for (int y = 0; y < maxY; y++)
                {
                    heightMap[maxX - (i + 1), y] = heightMap[maxX - (i + 2), y];
                }

                // Bottom
                for (int x = 0; x < maxX; x++)
                {
                    heightMap[x, maxY - (i + 1)] = heightMap[x, maxY - (i + 2)];
                }
            }
        }

        private void normalizeMap(float[,] heightMap)
        {
            for (int y = 0; y < heightMap.GetLength(1); y++)
            {
                for (int x = 0; x < heightMap.GetLength(0); x++)
                {
                    heightMap[x, y] = heightMap[x, y] / maxX;
                }
            }
        }
        // N/A 
        private float getRandomHeight()
        {
            return (float)rand.NextDouble() * maxHeight;
        }

        private float getRandomHeight(int max)
        {
            return (float)rand.NextDouble() * max * TERR_ROUGHNESS - TERR_GRADIENT*max;
        }

        // TODO: make csize constant with maxX

        /* Helper function to make the array index wrap around e.g arr[-1] == arr[length-1]*/
        private int wrapMod(int index)
        {
            return (Math.Abs(index * maxX) + index) % maxX;
        }

        private int wrapMod(int index, int csize)
        {
            return (Math.Abs(index * csize) + index) % csize;
        }
    }
}
